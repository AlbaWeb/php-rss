<?php 
// Link to your DB Config file
require_once('../inc/config.php');

// prepare output block
$item_block='';

// Query the source table for the content (Edit as you like...)
$query = $dbcon->query("SELECT * FROM `blog` ORDER BY `date` DESC LIMIT 0, 10");
if($query->num_rows > 0)
{
	
	while ($row = $query->fetch_assoc()) 
	{ 
		$postID = $row['id'];
		$title = mysqli_real_escape_string($dbcon, $row['title']);
		$date2 = gmdate(DATE_RSS, strtotime($row['date']));
		$description = $row['desc'];
					
		$item_block.='
		<item>
			<title>'.htmlspecialchars($title).'</title>
			<description>'.htmlspecialchars($description).'</description>
			<link>http://'.$siteURL.'read/index.php?title='.urlencode($title).'&amp;post='.$postID.'</link>
			<pubDate>'.$date2.'</pubDate>
			<guid>http://'.$siteURL.'read/post='.$postID.'</guid>

		</item>
		';
				
	}
}
header("Content-Type: application/rss+xml");
echo '<?xml version="1.0" encoding="utf-8"?>'; ?>
<rss version="2.0" xmlns:atom="http://www.w3.org/2005/Atom">

<channel>
	<title>my RSS Feed</title>
	<description>Description of the RSS feed</description>
	<link>http://www.yoursite.com</link>
	<atom:link href="LINK-TO-RSS-FEED.com" rel="self" type="application/rss+xml" />
	<?php 
		// 
		echo $item_block; 
	?>
</channel>
</rss>
