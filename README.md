# PHP Dynamic RSS Feed #

This is a simple base script to allow people who are creating a dynamic content that they wish to have an RSS feed. 

### What is this repository for? ###

Create an RSS feed for
* User Submitted Links
* Home made blog...
* News page

### How do I get set up? ###

* Upload this to a subfolder (yourdomain.com/rss/)
* Set your query for the feed in the RSS file. 
* go to somewhere like Feedburner (http://feedburner.com) and validate your feed. 

### Who do I talk to? ###

* I can be reached on any link on my homepage www.barrysmith.org
* Bugs can be submitted via the repo issue tracker https://bitbucket.org/AlbaWeb/php-rss/